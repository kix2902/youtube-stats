package com.redinput.youtubestats;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WidgetConfig extends Activity implements OnClickListener {

	private EditText txtCanal;
	private Button btnOK, btnCancelar;

	private int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.config);

		txtCanal = (EditText) findViewById(R.id.txtCanal);

		btnOK = (Button) findViewById(R.id.btnOK);
		btnCancelar = (Button) findViewById(R.id.btnCancelar);

		btnOK.setOnClickListener(this);
		btnCancelar.setOnClickListener(this);

		setResult(RESULT_CANCELED);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnOK:
			String canal = txtCanal.getText().toString().trim();

			if (canal.contains(" ")) {
				Toast.makeText(this, R.string.toastEspacio, Toast.LENGTH_SHORT).show();

			} else {
				SharedPreferences prefs = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
				Editor edit = prefs.edit();

				edit.putString("canal" + widgetId, canal);
				edit.commit();

				AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
				WidgetProvider.buildWidget(this, appWidgetManager, new int[] { widgetId });

				Intent result = new Intent();
				result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
				setResult(RESULT_OK, result);

				finish();
			}

			break;

		case R.id.btnCancelar:
			finish();

			break;
		}
	}
}
