package com.redinput.youtubestats;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.widget.RemoteViews;

import com.android.dataframework.DataFramework;
import com.android.dataframework.Entity;
import com.redinput.youtubestats.activities.ShowData;

public class WidgetProvider extends AppWidgetProvider {

	private static String RUTA_BASE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.MCS/";

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);

		buildWidget(context, appWidgetManager, appWidgetIds);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);

		for (int widgetId : appWidgetIds) {
			SharedPreferences prefs = context.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
			String canal = prefs.getString("canal" + widgetId, "");

			Editor edit = prefs.edit();
			edit.remove("canal" + widgetId);
			edit.commit();

			try {
				DataFramework.getInstance().open(context, context.getApplicationInfo().packageName);

				List<Entity> datosCanal = DataFramework.getInstance().getEntityList("stats", "canal='" + canal + "'");

				Iterator<Entity> iterator = datosCanal.iterator();
				while (iterator.hasNext()) {
					Entity entity = (Entity) iterator.next();

					entity.delete();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			DataFramework.getInstance().close();
		}
	}

	public static void buildWidget(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

		for (int widgetId : appWidgetIds) {
			if (isOnline(context)) {
				Intent intent = new Intent(context, WidgetService.class);
				intent.putExtra("widgetId", widgetId);

				context.startService(intent);

			} else {
				SharedPreferences prefs = context.getSharedPreferences("preferencias", Context.MODE_PRIVATE);
				final String canal = prefs.getString("canal" + widgetId, "");

				if (canal.length() > 0) {
					try {
						DataFramework.getInstance().open(context, context.getApplicationInfo().packageName);

						SimpleDateFormat sdfFecha = new SimpleDateFormat("yyyy-MM-dd");
						String fechaHoy = sdfFecha.format(new Date(System.currentTimeMillis()));

						Entity canalHoy = null;
						List<Entity> datosCanalHoy = DataFramework.getInstance().getEntityList("stats", "(canal='" + canal + "') AND (dia = '" + fechaHoy + "')");
						if (datosCanalHoy.size() > 0) {
							canalHoy = datosCanalHoy.get(0);
						}

						Entity canalAnterior = null;
						List<Entity> datosCanalAnterior = DataFramework.getInstance().getEntityList("stats", "(canal='" + canal + "') AND (dia != '" + fechaHoy + "')", "dia DESC");
						if (datosCanalAnterior.size() > 0) {
							canalAnterior = datosCanalAnterior.get(0);
						}

						String subs = "", views = "";
						if ((canalHoy != null) && (canalAnterior != null)) {
							subs = context.getString(R.string.subs) + ": " + readableNumber(canalHoy.getInt("subs")) + " [" + readableNumber((canalHoy.getInt("subs") - canalAnterior.getInt("subs")))
									+ "]";
							views = context.getString(R.string.visitas) + ": " + readableNumber(canalHoy.getInt("views")) + " ["
									+ readableNumber((canalHoy.getInt("views") - canalAnterior.getInt("views"))) + "]";

						} else if ((canalHoy != null) && (canalAnterior == null)) {
							subs = context.getString(R.string.subs) + ": " + readableNumber(canalHoy.getInt("subs")) + " [0]";
							views = context.getString(R.string.visitas) + ": " + readableNumber(canalHoy.getInt("views")) + " [0]";

						} else if ((canalHoy == null) && (canalAnterior != null)) {
							subs = context.getString(R.string.subs) + ": " + readableNumber(canalAnterior.getInt("subs")) + " [0]";
							views = context.getString(R.string.visitas) + ": " + readableNumber(canalAnterior.getInt("views")) + " [0]";

						} else {
							subs = context.getString(R.string.subs) + ": " + context.getString(R.string.no_data);
							views = context.getString(R.string.visitas) + ": " + context.getString(R.string.no_data);

						}

						RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.widget);

						view.setTextViewText(R.id.txtCanalWidget, canal);
						view.setTextViewText(R.id.txtSubsWidget, subs);
						view.setTextViewText(R.id.txtVisitasWidget, views);

						if (new File(RUTA_BASE + canal + ".png").exists()) {
							view.setImageViewUri(R.id.imgAvatarWidget, Uri.fromFile(new File(RUTA_BASE + canal + ".png")));
						}

						Intent intent = new Intent(context, ShowData.class);
						intent.putExtra("canal", canal);
						PendingIntent pIntent = PendingIntent.getActivity(context, widgetId, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

						view.setOnClickPendingIntent(R.id.layWidget, pIntent);

						appWidgetManager.updateAppWidget(widgetId, view);

					} catch (Exception e) {
						e.printStackTrace();
					}

					DataFramework.getInstance().close();
				}
			}
		}
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public static String readableNumber(long count) {
		if (count < 1000) {
			return "" + count;
		}
		int exp = (int) (Math.log(count) / Math.log(1000));
		return String.format("%.1f %c", count / Math.pow(1000, exp), "KMGTPE".charAt(exp - 1));
	}

}
