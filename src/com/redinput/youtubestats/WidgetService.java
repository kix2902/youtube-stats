package com.redinput.youtubestats;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.xmlpull.v1.XmlPullParserException;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.Environment;
import android.os.IBinder;
import android.widget.RemoteViews;

import com.android.dataframework.DataFramework;
import com.android.dataframework.Entity;
import com.redinput.youtubestats.activities.ShowData;

public class WidgetService extends Service {

	private static String URL_BASE = "http://gdata.youtube.com/feeds/api/users/";
	private static String RUTA_BASE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.MCS/";
	private static long diaMilis = 1000 * 60 * 60 * 24;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		final int widgetId = intent.getIntExtra("widgetId", AppWidgetManager.INVALID_APPWIDGET_ID);

		SharedPreferences prefs = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
		final String canal = prefs.getString("canal" + widgetId, "");

		if (canal.length() > 0) {
			final String url = URL_BASE + canal;

			new Thread(new Runnable() {

				@Override
				public void run() {

					try {
						DataFramework.getInstance().open(getApplicationContext(), getApplicationInfo().packageName);

						AndroidHttpClient cliente = AndroidHttpClient.newInstance("YouTubeStats/1.0/Android");
						HttpGet get = new HttpGet(url);
						HttpResponse response = cliente.execute(get);

						XmlParser xmlParser = new XmlParser();
						Entry entry = xmlParser.parse(response.getEntity().getContent());

						Bitmap avatar;
						new File(RUTA_BASE).mkdirs();
						if (!new File(RUTA_BASE + canal + ".png").exists()) {
							get = new HttpGet(entry.getAvatar());
							response = cliente.execute(get);

							avatar = BitmapFactory.decodeStream(response.getEntity().getContent());
							avatar.compress(CompressFormat.PNG, 100, new FileOutputStream(new File(RUTA_BASE + canal + ".png")));
							avatar.recycle();
						}

						cliente.close();

						SimpleDateFormat sdfFecha = new SimpleDateFormat("yyyy-MM-dd");
						String fechaHoy = sdfFecha.format(new Date(System.currentTimeMillis()));

						Entity canalHoy;
						List<Entity> datosCanalHoy = DataFramework.getInstance().getEntityList("stats", "(canal='" + canal + "') AND (dia = '" + fechaHoy + "')");
						if (datosCanalHoy.size() > 0) {
							canalHoy = datosCanalHoy.get(0);

						} else {
							canalHoy = new Entity("stats");
							canalHoy.setValue("canal", canal);
							canalHoy.setValue("dia", fechaHoy);
						}

						canalHoy.setValue("subs", entry.getSubs());
						canalHoy.setValue("views", entry.getViews());

						canalHoy.save();

						Date ayer = new Date(System.currentTimeMillis() - diaMilis);
						String fechaAyer = sdfFecha.format(ayer);

						int subsDif = 0, viewsDif = 0;

						List<Entity> datosCanalAyer = DataFramework.getInstance().getEntityList("stats", "(canal='" + canal + "') AND (dia = '" + fechaAyer + "')");
						if (datosCanalAyer.size() > 0) {
							Entity canalAyer = datosCanalAyer.get(0);

							subsDif = canalHoy.getInt("subs") - canalAyer.getInt("subs");
							viewsDif = canalHoy.getInt("views") - canalAyer.getInt("views");
						}

						canalHoy.setValue("subsDif", subsDif);
						canalHoy.setValue("viewsDif", viewsDif);

						canalHoy.save();

						String subs = getString(R.string.subs) + ": " + readableNumber(entry.getSubs()) + " [" + readableNumber(subsDif) + "]";
						String views = getString(R.string.visitas) + ": " + readableNumber(entry.getViews()) + " [" + readableNumber(viewsDif) + "]";

						RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget);

						view.setTextViewText(R.id.txtCanalWidget, canal);
						view.setTextViewText(R.id.txtSubsWidget, subs);
						view.setTextViewText(R.id.txtVisitasWidget, views);

						view.setImageViewUri(R.id.imgAvatarWidget, Uri.fromFile(new File(RUTA_BASE + canal + ".png")));

						Intent intent = new Intent(getApplicationContext(), ShowData.class);
						intent.putExtra("canal", canal);
						PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), widgetId, intent, Intent.FLAG_ACTIVITY_NEW_TASK);

						view.setOnClickPendingIntent(R.id.layWidget, pIntent);

						AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(WidgetService.this);
						appWidgetManager.updateAppWidget(widgetId, view);

					} catch (IOException e) {
						e.printStackTrace();

					} catch (IllegalStateException e) {
						e.printStackTrace();

					} catch (XmlPullParserException e) {
						e.printStackTrace();

					} catch (Exception e) {
						e.printStackTrace();
					}

					DataFramework.getInstance().close();
					stopSelf();
				}

			}).start();

		}

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public static String readableNumber(long count) {
		if (count < 1000) {
			return "" + count;
		}
		int exp = (int) (Math.log(count) / Math.log(1000));
		return String.format("%.1f %c", count / Math.pow(1000, exp), "KMGTPE".charAt(exp - 1));
	}

}
