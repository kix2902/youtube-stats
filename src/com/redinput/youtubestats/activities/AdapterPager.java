package com.redinput.youtubestats.activities;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.android.dataframework.Entity;
import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewStyle;
import com.jjoe64.graphview.LineGraphView;
import com.redinput.youtubestats.R;

public class AdapterPager extends PagerAdapter {

	private final static int NUM_COLUMNAS = 4;
	private Context context;
	private ArrayList<Entity> items;

	public AdapterPager(Context context, ArrayList<Entity> items) {
		this.context = context;
		this.items = items;
	}

	@Override
	public int getCount() {
		return NUM_COLUMNAS;
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		GraphViewData[] data = new GraphViewData[7];
		GraphView graphView = null;

		switch (position) {
		case 0:
			for (int i = items.size() - 1; i >= 0; i--) {
				data[i] = new GraphViewData(i, items.get(i).getInt("subs").doubleValue());
			}
			graphView = new LineGraphView(context, "");

			break;

		case 1:
			for (int i = items.size() - 1; i >= 0; i--) {
				data[i] = new GraphViewData(i, items.get(i).getInt("subsDif").doubleValue());
			}
			graphView = new BarGraphView(context, "");

			break;

		case 2:
			for (int i = items.size() - 1; i >= 0; i--) {
				data[i] = new GraphViewData(i, items.get(i).getInt("views").doubleValue());
			}
			graphView = new LineGraphView(context, "");

			break;

		case 3:
			for (int i = items.size() - 1; i >= 0; i--) {
				data[i] = new GraphViewData(i, items.get(i).getInt("viewsDif").doubleValue());
			}
			graphView = new BarGraphView(context, "");

			break;
		}

		data = ReverseData(data);

		GraphViewSeries dataSerie = new GraphViewSeries("data", new GraphViewStyle(Color.rgb(0, 0, 255), 3), data);
		graphView.addSeries(dataSerie);

		String[] labels = new String[items.size()];
		for (int i = 0; i < items.size(); i++) {
			String[] date = items.get(i).getString("dia").split("-");
			labels[items.size() - (i + 1)] = date[2] + "/" + date[1];
		}

		graphView.setHorizontalLabels(labels);
		graphView.setBackgroundColor(Color.rgb(240, 240, 255));
		graphView.setLabelColor(Color.BLACK);

		graphView.setShowLegend(false);

		((ViewPager) container).addView(graphView, 0);

		return graphView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object view) {
		((ViewPager) container).removeView((GraphView) view);
	}

	@Override
	public boolean isViewFromObject(View v, Object o) {
		return v == ((GraphView) o);
	}

	@Override
	public void finishUpdate(View arg0) {
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View arg0) {
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return context.getText(R.string.subs);

		case 1:
			return context.getText(R.string.subs) + " " + context.getText(R.string.dif);

		case 2:
			return context.getText(R.string.visitas);

		case 3:
			return context.getText(R.string.visitas) + " " + context.getText(R.string.dif);
		}

		return null;
	}

	private GraphViewData[] ReverseData(GraphViewData[] array) {
		int contador = 0;
		for (GraphViewData item : array) {
			if (item != null) {
				contador++;
			}
		}

		GraphViewData[] result = new GraphViewData[contador];

		int pos = 0;
		for (int i = array.length - 1; i >= 0; i--) {
			GraphViewData item = array[i];

			if (item != null) {
				result[pos] = item;
				pos++;
			}
		}

		return result;
	}
}
