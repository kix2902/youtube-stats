package com.redinput.youtubestats.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.android.dataframework.DataFramework;
import com.android.dataframework.Entity;
import com.redinput.youtubestats.R;
import com.viewpagerindicator.TitlePageIndicator;

public class ShowData extends SherlockActivity implements OnPageChangeListener {

	private static String RUTA_BASE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.MCS/";

	private List<Entity> listado;
	private String canal;
	private ActionBar actionBar;
	private TableLayout table;

	private AdapterPager adapter;
	private ViewPager pager;
	private int lastPos = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_data);

		actionBar = getSupportActionBar();

		canal = getIntent().getStringExtra("canal");
		actionBar.setTitle(canal);

		Drawable icon = BitmapDrawable.createFromPath(new File(RUTA_BASE + canal + ".png").toString());
		actionBar.setIcon(icon);

		try {
			DataFramework.getInstance().open(ShowData.this, getPackageName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		table = (TableLayout) findViewById(R.id.tblData);

		listado = DataFramework.getInstance().getEntityList("stats", "canal ='" + canal + "'", "dia DESC");

		ArrayList<Entity> data = new ArrayList<Entity>();

		int pos = 0;
		for (Entity item : listado) {
			TableRow tr = new TableRow(ShowData.this);

			TextView txtFecha = new TextView(ShowData.this);
			txtFecha.setText(item.getString("dia"));
			tr.addView(txtFecha);

			TextView txtSubs = new TextView(ShowData.this);
			txtSubs.setText(item.getInt("subs") + "");
			tr.addView(txtSubs);

			TextView txtSubsDif = new TextView(ShowData.this);
			txtSubsDif.setText(item.getInt("subsDif") + "");
			tr.addView(txtSubsDif);

			TextView txtViews = new TextView(ShowData.this);
			txtViews.setText(item.getInt("views") + "");
			tr.addView(txtViews);

			TextView txtViewsDif = new TextView(ShowData.this);
			txtViewsDif.setText(item.getInt("viewsDif") + "");
			tr.addView(txtViewsDif);

			table.addView(tr, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

			if (pos < 7) {
				data.add(item);
				pos++;
			}
		}

		adapter = new AdapterPager(ShowData.this, data);
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		TitlePageIndicator indicator = (TitlePageIndicator) findViewById(R.id.titles);
		indicator.setTextColor(Color.BLACK);
		indicator.setSelectedColor(Color.BLACK);
		indicator.setSelectedBold(true);
		indicator.setOnPageChangeListener(this);

		indicator.setViewPager(pager);

		highlightColumn(0);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		DataFramework.getInstance().close();
	}

	private void highlightColumn(int pos) {
		for (int i = 0; i < table.getChildCount(); i++) {
			((TextView) ((TableRow) table.getChildAt(i)).getChildAt(lastPos)).setTextAppearance(ShowData.this, R.style.normalText);
			((TextView) ((TableRow) table.getChildAt(i)).getChildAt(pos + 1)).setTextAppearance(ShowData.this, R.style.boldText);
		}

		lastPos = pos + 1;
	}

	@Override
	public void onPageSelected(int position) {
		highlightColumn(position);
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}
}
