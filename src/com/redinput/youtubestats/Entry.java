package com.redinput.youtubestats;

public class Entry {
	int subs, views;
	String avatar;

	public Entry() {
	}

	public int getSubs() {
		return subs;
	}

	public void setSubs(String subs) {
		this.subs = Integer.parseInt(subs);
	}

	public int getViews() {
		return views;
	}

	public void setViews(String views) {
		this.views = Integer.parseInt(views);
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

}
