package com.redinput.youtubestats;

import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class XmlParser {
	// We don't use namespaces
	private static final String ns = null;

	public Entry parse(InputStream in) throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setInput(in, null);
			parser.nextTag();
			return readEntry(parser);
		} finally {
			in.close();
		}
	}

	private Entry readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "entry");
		Entry entry = new Entry();
		while (parser.next() != XmlPullParser.END_DOCUMENT) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("statistics")) {
				entry.setSubs(parser.getAttributeValue(null, "subscriberCount"));
				entry.setViews(parser.getAttributeValue(null, "totalUploadViews"));

			} else if (name.equals("thumbnail")) {
				entry.setAvatar(parser.getAttributeValue(null, "url"));

			} else {
				skip(parser);
			}
		}
		return entry;
	}

	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}
}
